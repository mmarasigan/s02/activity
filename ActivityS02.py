# ACTIVITY S02


#1. Accept a year input from the user and determine if it is a leap year or not.
 

# checks if leap year----
year =  int(input ("Please input a year: "))

if int(year)>0  :
	if ( year % 4) == 0 and (year % 100) != 0 or (year % 400)==0 :
		print (f"The year {year} is a leap year")
				 
	else:     
		print (f"{year} is not a leap year")
else:
	print('Negative number or zero not allowed')


# -------------------
#2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

row = int(input ("Enter number of rows: \n"))
col = int(input ("Enter number of column: \n"))

r=1  #row 
c=1  #column

for r in range(row):
	for c in range(col):
		print("*", end=" ")
	print()



